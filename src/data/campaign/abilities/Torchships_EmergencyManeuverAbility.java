package data.campaign.abilities;

import java.awt.Color;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BattleAPI;
//import com.fs.starfarer.api.campaign.BuffManagerAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.AbilityPlugin;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberViewAPI;
import com.fs.starfarer.api.impl.campaign.abilities.BaseDurationAbility;
import com.fs.starfarer.api.impl.campaign.ids.Abilities;
//import com.fs.starfarer.api.impl.campaign.terrain.CRRecoveryBuff;
import com.fs.starfarer.api.ui.LabelAPI;
import com.fs.starfarer.api.ui.TooltipMakerAPI;
import com.fs.starfarer.api.util.Misc;
import java.util.ArrayList;
import java.util.List;

public class Torchships_EmergencyManeuverAbility extends BaseDurationAbility {

//	public static final float SENSOR_RANGE_MULT = 0.5f;
	private final float DETECTABILITY_PERCENT = 50f;
	private final float MAX_BURN_MOD = 7f;
	private final float CR_COST_MULT = 0.5f;
	private final float FUEL_USE_MULT = 0.25f;
	
	private final float ACCELERATION_MULT = 10f;
	
	
	
	public String getSpriteName() {
		return Global.getSettings().getSpriteName("abilities", "emergency_maneuver");
	}	

	@Override
	protected String getActivationText() {
		return "Emergency maneuver";
	}

	@Override
	protected void activateImpl() {
		
		AbilityPlugin goDark = entity.getAbility(Abilities.GO_DARK);
		if (goDark != null && goDark.isActive()) {
			goDark.deactivate();
		}
		AbilityPlugin em = entity.getAbility("emergency_burn");
		if (em != null && em.isActive()) {
			em.deactivate();
		}
		
		CampaignFleetAPI fleet = getFleet();
		if (fleet == null) return;
		
		for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
			float crLoss = member.getDeployCost() * CR_COST_MULT;
			member.getRepairTracker().applyCREvent(-crLoss, "Emergency maneuver");
		}
		
		float cost = computeFuelCost();
		fleet.getCargo().removeFuel(cost);
	}

	@Override
	protected void applyEffect(float level) {
		if (level > 0) {
			AbilityPlugin goDark = entity.getAbility(Abilities.GO_DARK);
			if (goDark != null) goDark.forceDisable();
//			AbilityPlugin em = entity.getAbility("emergency_burn");
//			if (em != null) em.forceDisable();
		}
		
		CampaignFleetAPI fleet = getFleet();
		if (fleet == null) return;
		
//		fleet.getStats().getSensorRangeMod().modifyMult(getModId(), 1f + (SENSOR_RANGE_MULT - 1f) * level, "Emergency maneuver");
		fleet.getStats().getDetectedRangeMod().modifyPercent(getModId(), DETECTABILITY_PERCENT * level, "Emergency maneuver");
		fleet.getStats().getFleetwideMaxBurnMod().modifyFlat(getModId(), (int)(MAX_BURN_MOD * level), "Emergency maneuver");
		fleet.getStats().getAccelerationMult().modifyMult(getModId(), 1f + (ACCELERATION_MULT - 1f) * level);
		
		for (FleetMemberViewAPI view : fleet.getViews()) {
//			view.getContrailColor().shift(getModId(), new Color(50,50,50,155), 1f, 1f, .75f);
//			view.getContrailColor().shift(getModId(), new Color(255,100,100), 1f, 1f, 0.25f);
//			view.getContrailDurMult().shift(getModId(), 1.5f, 1f, 1f, 0.5f);
//			view.getContrailWidthMult().shift(getModId(), 2f, 1f, 1f, 0.5f);
			view.getEngineGlowSizeMult().shift(getModId(), 1.5f, 1f, 1f, 1f);
			view.getEngineHeightMult().shift(getModId(), 3f, 1f, 1f, 1f);
			view.getEngineWidthMult().shift(getModId(), 2f, 1f, 1f, 1f);
		}
		
//		String buffId = getModId();
//		float buffDur = 0.1f;
//		boolean needsSync = false;
//		for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
//			if (level <= 0) {
//				member.getBuffManager().removeBuff(buffId);
//				needsSync = true;
//			} else {
//				BuffManagerAPI.Buff test = member.getBuffManager().getBuff(buffId);
//				if (test instanceof CRRecoveryBuff) {
//					CRRecoveryBuff buff = (CRRecoveryBuff) test;
//					buff.setDur(buffDur);
//				} else {
//					member.getBuffManager().addBuff(new CRRecoveryBuff(buffId, 0f, buffDur));
//					needsSync = true;
//				}
//			}
//		}
		
//		if (needsSync || fleet.isPlayerFleet()) {
//			fleet.forceSync();
//		}
	}

	@Override
	protected void deactivateImpl() {
		cleanupImpl();
	}
	
	@Override
	protected void cleanupImpl() {
		CampaignFleetAPI fleet = getFleet();
		if (fleet == null) return;
		
		fleet.getStats().getSensorRangeMod().unmodify(getModId());
		fleet.getStats().getDetectedRangeMod().unmodify(getModId());
		fleet.getStats().getFleetwideMaxBurnMod().unmodify(getModId());
		fleet.getStats().getAccelerationMult().unmodify(getModId());
	}
	
	@Override
	public float getActivationDays() {
		return 0.05f;
	}

	@Override
	public float getCooldownDays() {
		return 1f;
	}

	@Override
	public float getDeactivationDays() {
		return 0.15f;
	}

	@Override
	public float getDurationDays() {
		return 0.2f;
	}

	@Override
	public boolean isUsable() {
		return super.isUsable() && 
					getFleet() != null && 
					getNonReadyShips().isEmpty() &&
					(getFleet().isAIMode() || computeFuelCost() <= getFleet().getCargo().getFuel());
	}
	
	protected List<FleetMemberAPI> getNonReadyShips() {
		List<FleetMemberAPI> result = new ArrayList<FleetMemberAPI>();
		CampaignFleetAPI fleet = getFleet();
		if (fleet == null) return result;
		
		for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
			float crLoss = member.getDeployCost() * CR_COST_MULT;
			if (Math.round(member.getRepairTracker().getCR() * 100) < Math.round(crLoss * 100)) {
				result.add(member);
			}
		}
		return result;
	}

	protected float computeFuelCost() {
		CampaignFleetAPI fleet = getFleet();
		if (fleet == null) return 0f;
		
		float cost = fleet.getLogistics().getFuelCostPerLightYear() * FUEL_USE_MULT;
		return cost;
	}
	
	protected float computeSupplyCost() {
		CampaignFleetAPI fleet = getFleet();
		if (fleet == null) return 0f;
		
		float cost = 0f;
		for (FleetMemberAPI member : fleet.getFleetData().getMembersListCopy()) {
			cost += member.getDeploymentPointsCost() * CR_COST_MULT;
		}
		return cost;
	}

	
	@Override
	public void createTooltip(TooltipMakerAPI tooltip, boolean expanded) {
		CampaignFleetAPI fleet = getFleet();
		if (fleet == null) return;
		
		Color gray = Misc.getGrayColor();
		Color highlight = Misc.getHighlightColor();
		Color fuel = Global.getSettings().getColor("progressBarFuelColor");
		Color bad = Misc.getNegativeHighlightColor();
		
		LabelAPI title = tooltip.addTitle("Emergency maneuver");

		float pad = 10f;
		
		float fuelCost = computeFuelCost();
		float supplyCost = computeSupplyCost();
		
		tooltip.addPara("Consumes %s fuel and reduces the combat readiness" +
					" of all ships, costing up to %s supplies to recover.", pad, 
					highlight,
					Misc.getRoundedValueMaxOneAfterDecimal(fuelCost),
					Misc.getRoundedValueMaxOneAfterDecimal(supplyCost));
		
		if (fuelCost > fleet.getCargo().getFuel()) {
			tooltip.addPara("Not enough fuel.", bad, pad);
		}
		
		List<FleetMemberAPI> nonReady = getNonReadyShips();
		if (!nonReady.isEmpty()) {
			tooltip.addPara("Not all ships have enough combat readiness to initiate an emergency maneuver. CR required:", bad, pad);
			tooltip.beginGridFlipped(getTooltipWidth(), 1, 30, pad);
			tooltip.setGridLabelColor(bad);
			int j = 0;
			for (FleetMemberAPI member : nonReady) {
				float crLoss = member.getDeployCost() * CR_COST_MULT;
				String cost = "" + Math.round(crLoss * 100) + "%";
				String str = "";
				if (!member.isFighterWing()) {
					str += member.getShipName() + ", ";
					str += member.getHullSpec().getHullName() + "-class";
				} else {
					str += member.getVariant().getFullDesignationWithHullName();
				}
				tooltip.addToGrid(0, j++, str, cost, bad);
			}
			tooltip.addGrid(3f);
		}
		
		tooltip.addPara("Increases the maximum burn level by %s." +
				" Dramaticaly increases the acceleration." +
				" Increases the range at which the fleet" +
				" can be detected by %s." + 
                                " Last a quarter of a day.", pad,
                                
				highlight, 
				"" + (int) MAX_BURN_MOD,
				"" + (int)(DETECTABILITY_PERCENT) + "%"
		);
		tooltip.addPara("Disables \"Go Dark\" when activated.", pad);
	}

	public boolean hasTooltip() {
		return true;
	}
	
	@Override
	public boolean isTooltipExpandable() {
		return false;
	}


	@Override
	public void fleetLeftBattle(BattleAPI battle, boolean engagedInHostilities) {
		if (engagedInHostilities) {
			deactivate();
		}
	}

	@Override
	public void fleetOpenedMarket(MarketAPI market) {
		deactivate();
	}
	
}






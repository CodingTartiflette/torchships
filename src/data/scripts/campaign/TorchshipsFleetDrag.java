//by Tartiflette, reduce the acceleration of nearby fleets along the number and size of their members, ships with the slowest burn rate count double.
package data.scripts.campaign;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import java.util.List;
//import org.apache.log4j.Logger;
import org.lazywizard.lazylib.campaign.CampaignUtils;

public class TorchshipsFleetDrag implements EveryFrameScript {    
    
    private final String ID="Fleet Drag";
    private float tik=0f, tok=0.5f;
//    public static Logger log = Global.getLogger(TorchshipsFleetDrag.class);
    private final float SLOW_SHIPS_INTERFERENCE = 0.33f, FLEET_SIZE_INTERFERENCE = 0.33f;
    
    @Override
    public void advance(float amount) {
        tik+=amount;
        
        if(tik>=tok){
            tik=0;
            
            float burn=20, member=1;
            
            //find nearby fleets
            CampaignFleetAPI player = Global.getSector().getPlayerFleet();
            List <CampaignFleetAPI> checkFleets = CampaignUtils.getNearbyFleets(player, 4000);
            checkFleets.add(player);
            //for each fleet
            for(CampaignFleetAPI fleet : checkFleets){
                for (FleetMemberAPI s : fleet.getFleetData().getMembersListCopy()){  
                    //find the number of slowest ships
                    float b = s.getStats().getMaxBurnLevel().getModifiedValue();
                    if (b<burn){
                        burn=b;
                        member=1;
                    } else if (b==burn){
                        member++;
                    }
                }
                //calculate the fleet size drag
                float fleetSizeDrag = Math.min(player.getFleetSizeCount(), 75)/75;
                //calculate the slow ships drag
                float slowShipsFormation = (float)Math.sin((Math.min(20, member)-1)*(Math.PI/38));
                //apply drag to the acceleration
                fleet.getStats().getAccelerationMult().modifyMult(ID, 1 - (SLOW_SHIPS_INTERFERENCE*  slowShipsFormation) - (FLEET_SIZE_INTERFERENCE * fleetSizeDrag));                
                
//            log.info("slowest members = "+member); 
//            log.info("Slow ships formation keeping = "+enginesInterferences);
//            log.info("Fleet size interferences = "+fleetSizeDrag);
//            log.info("Fleet acceleration  = "+ (SLOW_SHIPS_FORMATION * enginesInterferences)+(FLEET_SIZE_INTERFERENCE * fleetSizeDrag));
            }          
        }
    }
    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public boolean runWhilePaused() {
        return false;
    }
}
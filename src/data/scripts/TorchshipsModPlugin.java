package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import data.scripts.campaign.TorchshipsFleetDrag;


public class TorchshipsModPlugin extends BaseModPlugin {
    
    @Override
    public void onApplicationLoad() throws ClassNotFoundException {
        //check for lazylib
        try {
            Global.getSettings().getScriptClassLoader().loadClass("org.lazywizard.lazylib.ModUtils");
        } catch (ClassNotFoundException ex) {
            String message = System.lineSeparator()
                    + System.lineSeparator() + "LazyLib is required to run at least one of the mods you have installed."
                    + System.lineSeparator() + System.lineSeparator()
                    + "You can download LazyLib at http://fractalsoftworks.com/forum/index.php?topic=5444"
                    + System.lineSeparator();
            throw new ClassNotFoundException(message);
        }
    }
    
    @Override
    public void onNewGame() {
        Global.getSector().addScript(new TorchshipsFleetDrag());
    }
    
    public void onGameLoad() {       
        Global.getSector().addScript(new TorchshipsFleetDrag());
    }
}
